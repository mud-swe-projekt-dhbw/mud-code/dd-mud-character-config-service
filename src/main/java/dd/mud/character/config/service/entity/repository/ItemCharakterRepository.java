package dd.mud.character.config.service.entity.repository;

import dd.mud.character.config.service.entity.model.ItemCharakter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemCharakterRepository extends CrudRepository<ItemCharakter, Long> {

    List<ItemCharakter> findAllByChId (Long chId);
}