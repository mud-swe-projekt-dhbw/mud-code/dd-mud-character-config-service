package dd.mud.character.config.service.entity.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Getter
@AllArgsConstructor
@IdClass(ItemCharakterId.class)
@Table(name = "itemCharakter", schema = "public")
public class ItemCharakter {

    @Id
    Long chId;

    @Id
    Long itemId;

    Long gameId;
}
