package dd.mud.character.config.service.boundary;

import dd.mud.character.config.service.boundary.model.CharakterAlreadyExistsDTO;
import dd.mud.character.config.service.boundary.model.CharakterDTO;
import dd.mud.character.config.service.boundary.model.GameInformationDTO;
import dd.mud.character.config.service.controll.service.CharakterAlreadyExistsService;
import dd.mud.character.config.service.controll.service.CharakterSaveService;
import dd.mud.character.config.service.controll.service.DeleteCharakterService;
import dd.mud.character.config.service.controll.service.GameInformationRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/characterConfiguration")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CharakterConfigAPI {

    private final GameInformationRepositoryService gameInformationRepositoryService;
    private final CharakterSaveService charakterSaveService;
    private final CharakterAlreadyExistsService charakterAlreadyExistsService;
    private final DeleteCharakterService deleteCharakterService;

    /**
     * REST-call for Charakterkonfiguration: Game-Information: Races and Classes
     * @param gameId
     * @return GameInformationDTO
     */

    @GetMapping(value = "/gameId/{game-id}")
    public ResponseEntity<GameInformationDTO> getCharakterConfigurationInformation(@PathVariable(value = "game-id") Long gameId) {
        return ResponseEntity.ok().body(gameInformationRepositoryService.getGameInformationByGameId(gameId));
    }

    /**
     * REST-call for Charakterkonfiguration: Save CharakterDTO to DB
     * @param gameId, charakterDTO
     * @return HTTP-Answer created
     */

    @PostMapping(value = "/gameId/{game-id}/new")
    public ResponseEntity<CharakterDTO> saveNewCharakter(@PathVariable(value = "game-id", required = true) Long gameId, @RequestBody CharakterDTO charakterDTO) {
       charakterSaveService.saveCharakterToDB(charakterDTO,gameId);
        return ResponseEntity.created(null).build();
    }

    /**
     * REST-call for Charakterkonfiguration: Check if Player has already a Charakter for that Game
     * @param playerId, gameId
     * @return HTTP-Answer correct and CharakterAlreadyExistsDTO
     */

    @GetMapping(value = "/gameId/{game-id}/playerId/{player-id}/characterExistance")
    public ResponseEntity<CharakterAlreadyExistsDTO> playerHasAlreadyCharakter(@PathVariable(value = "game-id") Long gameId, @PathVariable(value = "player-id") Long playerId) {
        return ResponseEntity.ok().body(charakterAlreadyExistsService.charakterAlreadyExists(gameId, playerId));
    }

    /**
     * REST-call for Charakterkonfiguration: Delete Charakter from DB
     * @param gameId
     * @param  playerId
     * @return HTTP-Answer correct
     */

    @DeleteMapping(value = "/delete/gameId/{game-id}/playerId/{player-Id}")
    public ResponseEntity<CharakterDTO> deleteCharakter(@PathVariable(value = "game-id", required = true) Long gameId,@PathVariable(value = "player-Id", required = true) Long playerId) {
        deleteCharakterService.deleteCharakterWithGameIdAndPlayerId(gameId, playerId);
        return ResponseEntity.ok().build();
    }
}
