package dd.mud.character.config.service.boundary.model;

import lombok.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Data
@Builder
public class CharakterDTO {

    private String chName;

    private Long classId;

    private Long raceId;

    private Long itemId;

    private Long userId;

    private String description;

    private String personality;
}
