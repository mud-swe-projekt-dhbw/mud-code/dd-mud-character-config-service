package dd.mud.character.config.service.boundary;

import dd.mud.character.config.service.controll.exceptions.IllegalCharakterInformationException;
import dd.mud.character.config.service.controll.exceptions.IllegalGameIdException;
import dd.mud.character.config.service.controll.exceptions.IllegalUserIdException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
public class CharakterConfigAPIExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Void> handleUnexpectedException(Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Void> handleUnexpectedException(HttpMessageNotReadableException e) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalCharakterInformationException.class)
    public ResponseEntity<Void> handleUnexpectedException(IllegalCharakterInformationException e) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalGameIdException.class)
    public ResponseEntity<Void> handleUnexpectedException(IllegalGameIdException e) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Void> handleUnexpectedException(MethodArgumentTypeMismatchException e) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalUserIdException.class)
    public ResponseEntity<Void> handleUnexpectedException(IllegalUserIdException e) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
