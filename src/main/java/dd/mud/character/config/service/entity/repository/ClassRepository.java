package dd.mud.character.config.service.entity.repository;

import dd.mud.character.config.service.entity.model.Class;
import dd.mud.character.config.service.entity.model.Race;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassRepository extends CrudRepository<Class, Long> {

    Iterable<Class> findAllByGameId (Long gameId);
}
