package dd.mud.character.config.service.entity.model;

import lombok.*;

import javax.persistence.*;

// noArgsConstructor is necessary for the Database
@Entity
@NoArgsConstructor
@Getter
@AllArgsConstructor
@Table(name = "item", schema = "public")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String itemType;

    private String itemName;

    private Integer itemValue;

    private Long gameId;
}
