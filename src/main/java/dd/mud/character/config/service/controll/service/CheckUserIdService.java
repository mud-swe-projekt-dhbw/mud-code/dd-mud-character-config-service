package dd.mud.character.config.service.controll.service;

import dd.mud.character.config.service.controll.service.repositoryservices.UserRepositoryService;
import dd.mud.character.config.service.entity.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CheckUserIdService {

    private final UserRepositoryService userRepositoryService;

    /**
     * Checks if the UserId is valid
     * @param userId the UserId to check
     * @return boolean
     */
    public boolean existsInDatabase(Long userId){
        List<User> userList = userRepositoryService.getUsers();
        for (User user: userList) {
            if (userId.equals(user.getId())){
                return true;
            }
        }
        return false;
    }
}
