package dd.mud.character.config.service.controll.service.repositoryservices;
import dd.mud.character.config.service.entity.model.Charakter;
import dd.mud.character.config.service.entity.repository.CharakterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CharakterRepositoryService {

    private final CharakterRepository charakterRepository;

    public void saveNewCharakter(Charakter charakter){
            charakterRepository.save(charakter);
    }

    public List<Charakter> getAllCharakters(){
        List<Charakter> characterList = new ArrayList<>();
        charakterRepository.findAll().forEach(characterList::add);
        return characterList;
    }

    public boolean existCharakterByGameIdAndUserId(Long gameId, Long userId){
        return charakterRepository.existsByUserIdAndGameId(userId, gameId);
    }

    public Charakter getCharakterByUserIdAndGameId(Long userId, Long gameId){
        return charakterRepository.getCharakterByUserIdAndGameId(userId, gameId);
    }

    public void deleteCharakterById(Long id){
        charakterRepository.deleteById(id);
    }
}
