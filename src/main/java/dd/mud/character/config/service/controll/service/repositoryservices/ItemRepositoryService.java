package dd.mud.character.config.service.controll.service.repositoryservices;

import dd.mud.character.config.service.entity.model.Item;
import dd.mud.character.config.service.entity.repository.ItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ItemRepositoryService {

    private final ItemRepository itemRepository;

    public List<Item> getItemNamesByGameId (Long gameId) {
        List<Item> items = new ArrayList<>();
        itemRepository.findAllByGameId(gameId).forEach(items::add);
        return items;
    }
}
