package dd.mud.character.config.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DdMudCharacterConfigServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DdMudCharacterConfigServiceApplication.class, args);
    }

}
