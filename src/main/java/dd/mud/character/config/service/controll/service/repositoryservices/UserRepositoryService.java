package dd.mud.character.config.service.controll.service.repositoryservices;

import dd.mud.character.config.service.entity.model.User;
import dd.mud.character.config.service.entity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class UserRepositoryService {

    private final UserRepository userRepository;

    public List<User> getUsers () {
        return userRepository.findAll();
    }
}
