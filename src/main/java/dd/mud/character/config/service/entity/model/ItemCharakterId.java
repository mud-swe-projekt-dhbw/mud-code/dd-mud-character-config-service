package dd.mud.character.config.service.entity.model;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class ItemCharakterId implements Serializable {

        private Long chId;

        private Long itemId;
}
