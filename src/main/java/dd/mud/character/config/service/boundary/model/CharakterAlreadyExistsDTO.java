package dd.mud.character.config.service.boundary.model;

import lombok.*;

//@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Getter
public class CharakterAlreadyExistsDTO {

    boolean charakterAlreadyExists;
}
