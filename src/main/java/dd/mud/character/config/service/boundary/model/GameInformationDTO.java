package dd.mud.character.config.service.boundary.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@Getter
@Builder
public class GameInformationDTO {

    List<ClassDTO> classDTOList;

    List<RaceDTO> raceDTOList;

    List<ItemDTO> itemDTOList;
}
