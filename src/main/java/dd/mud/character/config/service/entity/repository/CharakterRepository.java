package dd.mud.character.config.service.entity.repository;

import dd.mud.character.config.service.entity.model.Charakter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharakterRepository extends CrudRepository<Charakter, Long> {

    boolean existsByChName(String chName);
    boolean existsByUserIdAndGameId(Long userId, Long gameId);
    Charakter getCharakterByUserIdAndGameId(Long userId, Long gameId);
}
