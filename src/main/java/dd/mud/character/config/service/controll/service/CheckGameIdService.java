package dd.mud.character.config.service.controll.service;

import dd.mud.character.config.service.controll.service.repositoryservices.GameRepositoryService;
import dd.mud.character.config.service.entity.model.Game;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CheckGameIdService {

    private final GameRepositoryService gameRepositoryService;

    /**
     * Checks if the GameId is valid
     * @param gameId the GameId to check
     * @return boolean
     */
    public boolean existsInDataBase(Long gameId){
        List<Game> gameList = gameRepositoryService.getGames();
        for (Game game: gameList) {
            if (gameId.equals(game.getId())){
                return true;
            }
        }
        return false;
    }
}
