package dd.mud.character.config.service.controll.service.repositoryservices;

import dd.mud.character.config.service.entity.model.Race;
import dd.mud.character.config.service.entity.repository.RaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RaceRepositoryService {

    private final RaceRepository raceRepository;

    public List<Race> getRaceNamesByGameId (Long gameId) {
        List<Race> races = new ArrayList<>();
        raceRepository.findAllByGameId(gameId).forEach(races::add);
        return races;
    }
}
