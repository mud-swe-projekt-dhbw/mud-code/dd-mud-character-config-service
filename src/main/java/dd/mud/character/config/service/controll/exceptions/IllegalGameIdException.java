package dd.mud.character.config.service.controll.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class IllegalGameIdException extends RuntimeException{
}
