package dd.mud.character.config.service.entity.model;

import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Builder
@AllArgsConstructor
@Table(name = "user_data", schema = "public")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String mail;

    private String username;

    private String mailHash;

    private String passwordHash;

    private boolean registrationStatus;

    private Long rank;
}
