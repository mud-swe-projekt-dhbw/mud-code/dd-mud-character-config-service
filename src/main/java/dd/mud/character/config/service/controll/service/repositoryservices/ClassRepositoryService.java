package dd.mud.character.config.service.controll.service.repositoryservices;

import dd.mud.character.config.service.entity.model.Class;
import dd.mud.character.config.service.entity.repository.ClassRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ClassRepositoryService {

    private final ClassRepository classRepository;

    public List<Class> getClassNamesByGameId (Long gameId) {
        List<Class> classes = new ArrayList<>();
        classRepository.findAllByGameId(gameId).forEach(classes::add);
        return classes;
    }
}
