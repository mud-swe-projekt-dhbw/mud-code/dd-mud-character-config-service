package dd.mud.character.config.service.controll.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class IllegalCharakterInformationException extends RuntimeException{
}
