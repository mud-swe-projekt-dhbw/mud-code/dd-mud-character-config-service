package dd.mud.character.config.service.boundary.model;

import lombok.*;

@AllArgsConstructor
@Data
@Builder
public class RaceDTO {

    Long raceID;

    String raceName;
}
