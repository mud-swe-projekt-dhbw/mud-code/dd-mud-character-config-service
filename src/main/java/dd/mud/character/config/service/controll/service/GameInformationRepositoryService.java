package dd.mud.character.config.service.controll.service;

import dd.mud.character.config.service.boundary.model.ClassDTO;
import dd.mud.character.config.service.boundary.model.GameInformationDTO;
import dd.mud.character.config.service.boundary.model.ItemDTO;
import dd.mud.character.config.service.boundary.model.RaceDTO;
import dd.mud.character.config.service.controll.exceptions.IllegalGameIdException;
import dd.mud.character.config.service.controll.service.repositoryservices.ClassRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.ItemRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.RaceRepositoryService;
import dd.mud.character.config.service.entity.model.Class;
import dd.mud.character.config.service.entity.model.Item;
import dd.mud.character.config.service.entity.model.Race;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameInformationRepositoryService {

    private final ClassRepositoryService classRepositoryService;
    private final RaceRepositoryService raceRepositoryService;
    private final ItemRepositoryService itemRepositoryService;
    private final CheckGameIdService checkGameIdService;

    /**
     * Provides an GameInformationDTO with all relevant data which are needed for the character configuration
     * @param gameId the GameId
     * @return GameInformationDTO with relevant data for character configuration
     */
    public GameInformationDTO getGameInformationByGameId (Long gameId){
        if(!(checkGameIdService.existsInDataBase(gameId))){
            throw new IllegalGameIdException();
        }

        List<ClassDTO> classDTOList = getClassDTOByGameId(gameId);
        List<RaceDTO> raceDTOList = getRaceDTOByGameId(gameId);
        List<ItemDTO> itemDTOList = getItemDTOByGameId(gameId);
        GameInformationDTO gameInformationDTO = new GameInformationDTO(classDTOList, raceDTOList, itemDTOList);
        return gameInformationDTO;
    }

    /**
     * Returns the classes from the database depending on the GameID
     * @param gameId the GameId
     * @return List<ClassDTO> List of classes with given GameID
     */
    public List<ClassDTO> getClassDTOByGameId (Long gameId){
        List<Class> classList = classRepositoryService.getClassNamesByGameId(gameId);
        List<ClassDTO> classDTOList = new ArrayList<>();
        for(Class clazz: classList){
            classDTOList.add(ClassDTO.builder().classID(clazz.getId()).className(clazz.getClassName()).build());
        }
        return  classDTOList;
    }

    /**
     * Returns the races from the database depending on the GameID
     * @param gameId the GameId
     * @return List<RaceDTO> List of races with given GameID
     */
    public List<RaceDTO> getRaceDTOByGameId (Long gameId){
        List<Race> raceList = raceRepositoryService.getRaceNamesByGameId(gameId);
        List<RaceDTO> raceDTOList = new ArrayList<>();
        for(Race race: raceList){
            raceDTOList.add(RaceDTO.builder().raceID(race.getId()).raceName(race.getRaceName()).build());
        }
        return raceDTOList;
    }

    /**
     * Returns the Items from the database depending on the GameID
     * @param gameId the GameId
     * @return List<ItemDTO> List of races with given GameID
     */
    public List<ItemDTO> getItemDTOByGameId (Long gameId){
        List<Item> itemList = itemRepositoryService.getItemNamesByGameId(gameId);
        List<ItemDTO> itemDTOList = new ArrayList<>();
        for (Item item : itemList){
            itemDTOList.add(ItemDTO.builder().itemId(item.getId()).itemName(item.getItemName()).build());
        }
        return itemDTOList;
    }
}
