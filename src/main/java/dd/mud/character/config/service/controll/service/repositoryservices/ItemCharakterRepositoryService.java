package dd.mud.character.config.service.controll.service.repositoryservices;

import dd.mud.character.config.service.entity.model.ItemCharakter;
import dd.mud.character.config.service.entity.repository.ItemCharakterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ItemCharakterRepositoryService {

    private final ItemCharakterRepository itemCharakterRepository;

    public void saveNewItemIdCharakterId(Long chId, Long itemId, Long gameId){
        itemCharakterRepository.save(new ItemCharakter(chId, itemId, gameId));
    }

    public void deleteItemIdCharakterId(Long chId, Long itemId, Long gameId){
        ItemCharakter itemCharakter = new ItemCharakter(chId, itemId, gameId);
        itemCharakterRepository.delete(itemCharakter);
    }

    public List<ItemCharakter> getItemCharakterByChId(Long chId){
        List<ItemCharakter> itemCharakterList = itemCharakterRepository.findAllByChId(chId);
        return itemCharakterList;
    }
}
