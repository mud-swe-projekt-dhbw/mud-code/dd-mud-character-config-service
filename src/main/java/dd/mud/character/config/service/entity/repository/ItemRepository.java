package dd.mud.character.config.service.entity.repository;

import dd.mud.character.config.service.entity.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

   Iterable<Item> findAllByGameId(Long gameId);
}
