package dd.mud.character.config.service.controll.service;

import dd.mud.character.config.service.controll.exceptions.IllegalGameIdException;
import dd.mud.character.config.service.controll.exceptions.IllegalUserIdException;
import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.ItemCharakterRepositoryService;
import dd.mud.character.config.service.entity.model.Charakter;
import dd.mud.character.config.service.entity.model.ItemCharakter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DeleteCharakterService {

    private final CharakterRepositoryService charakterRepositoryService;
    private final ItemCharakterRepositoryService itemCharakterRepositoryService;
    private final CheckGameIdService checkGameIdService;
    private final CheckUserIdService checkUserIdService;

    /**
     * Deletes a Charakter from the Database depends on GameId and UserId
     * @param gameId
     * @param  playerId
     */
    public void deleteCharakterWithGameIdAndPlayerId(Long gameId, Long playerId){
        if(!(checkGameIdService.existsInDataBase(gameId))){
            throw new IllegalGameIdException();
        }
        if(!(checkUserIdService.existsInDatabase(playerId))){
            throw new IllegalUserIdException();
        }
        if(!(charakterRepositoryService.existCharakterByGameIdAndUserId(gameId, playerId))){
            throw new IllegalGameIdException();
        }
        Charakter charakter = charakterRepositoryService.getCharakterByUserIdAndGameId(playerId, gameId);
        List<ItemCharakter> itemCharakterList = itemCharakterRepositoryService.getItemCharakterByChId(charakter.getId());
        for (ItemCharakter itemCharakter: itemCharakterList) {
            itemCharakterRepositoryService.deleteItemIdCharakterId(charakter.getId(), itemCharakter.getItemId(), gameId);
        }
        charakterRepositoryService.deleteCharakterById(charakter.getId());
    }
}
