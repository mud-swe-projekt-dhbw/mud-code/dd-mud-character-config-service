package dd.mud.character.config.service.controll.service;

import dd.mud.character.config.service.boundary.model.CharakterDTO;
import dd.mud.character.config.service.controll.exceptions.IllegalCharakterInformationException;
import dd.mud.character.config.service.controll.exceptions.IllegalGameIdException;
import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.ItemCharakterRepositoryService;
import dd.mud.character.config.service.entity.model.Charakter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Objects;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CharakterSaveService {

    private static final Long DEFAULT_HP = 1000L;
    private static final Long DEFAULT_DMG = 100L;

    private final CharakterRepositoryService charakterRepositoryService;
    private final ItemCharakterRepositoryService itemCharakterRepositoryService;
    private final CheckGameIdService checkGameIdService;

    /**
     * Converts the CharacterDTO into a Character-Object and saves it in the database
     * @param charakterDTO the character that is stored in the database
     * @param gameId to identify the Game
     */
    public void saveCharakterToDB(CharakterDTO charakterDTO, Long gameId){
        if ((checkIfNull(charakterDTO.getChName(), charakterDTO.getClassId(), charakterDTO.getRaceId(), charakterDTO.getItemId(),
                charakterDTO.getUserId(), charakterDTO.getDescription(), charakterDTO.getPersonality()))
                || checkIfEmptyString(charakterDTO.getChName(), charakterDTO.getDescription(), charakterDTO.getPersonality()) ){
            throw new IllegalCharakterInformationException();
        }

        if(!(checkGameIdService.existsInDataBase(gameId))){
            throw new IllegalGameIdException();
        }

        Charakter charakter = buildCharakter(charakterDTO, gameId);

        charakterRepositoryService.saveNewCharakter(charakter);
        Charakter charakterForId = charakterRepositoryService.getCharakterByUserIdAndGameId(charakter.getUserId(), charakter.getGameId());
        itemCharakterRepositoryService.saveNewItemIdCharakterId(charakterForId.getId(), charakterDTO.getItemId(), gameId);
    }

    /**
     * Checks whether the character object is complete
     * @return true if the CharakterDTO is not complete
     *         false if the CharakterDTO is complete
     */
    public Charakter buildCharakter(CharakterDTO charakterDTO, Long gameId){
        return Charakter.builder()
                .chName(charakterDTO.getChName())
                .chLevel(null)  //null because of default in the DB
                .hp(DEFAULT_HP)
                .damage(DEFAULT_DMG)
                .classId(charakterDTO.getClassId())
                .raceId(charakterDTO.getRaceId())
                .userId(charakterDTO.getUserId())
                .gameId(gameId)
                .description(charakterDTO.getDescription())
                .personality(charakterDTO.getPersonality())
                .build();
    }

    private boolean checkIfNull (Object ... objects){
        return Arrays.stream(objects).anyMatch(Objects::isNull);
    }

    private boolean checkIfEmptyString (String ... strings){
        return Arrays.stream(strings).anyMatch(String::isEmpty);
    }
}
