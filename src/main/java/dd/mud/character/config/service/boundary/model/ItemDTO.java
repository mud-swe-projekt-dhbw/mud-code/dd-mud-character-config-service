package dd.mud.character.config.service.boundary.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@Builder
public class ItemDTO {

    String itemName;

    Long itemId;
}
