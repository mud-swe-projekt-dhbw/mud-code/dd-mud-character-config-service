package dd.mud.character.config.service.entity.repository;

import dd.mud.character.config.service.entity.model.Race;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RaceRepository extends CrudRepository<Race, Long> {

    Iterable<Race> findAllByGameId (Long gameId);
}