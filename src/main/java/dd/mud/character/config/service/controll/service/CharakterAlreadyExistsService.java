package dd.mud.character.config.service.controll.service;

import dd.mud.character.config.service.boundary.model.CharakterAlreadyExistsDTO;
import dd.mud.character.config.service.controll.exceptions.IllegalGameIdException;
import dd.mud.character.config.service.controll.exceptions.IllegalUserIdException;
import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CharakterAlreadyExistsService {

    private final CharakterRepositoryService charakterRepositoryService;
    private final CheckGameIdService checkGameIdService;
    private final CheckUserIdService checkUserIdService;

    /**
     * Checks whether a character with a given gameId and userId exists in the database
     * @param gameId to identify the Character
     * @param userId to identify the Character
     * @return CharakterAlreadyExistsDTO DataTransferObject with the Information whether the character exists in the database
     */
    public CharakterAlreadyExistsDTO charakterAlreadyExists (Long gameId, Long userId){
        if(!(checkGameIdService.existsInDataBase(gameId))){
            throw new IllegalGameIdException();
        }
        if(!(checkUserIdService.existsInDatabase(userId))){
            throw new IllegalUserIdException();
        }
        boolean exists = charakterRepositoryService.existCharakterByGameIdAndUserId(gameId, userId);
        return new CharakterAlreadyExistsDTO(exists);
    }
}
