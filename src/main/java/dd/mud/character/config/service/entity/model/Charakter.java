package dd.mud.character.config.service.entity.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Builder
@Data
@AllArgsConstructor
@Table(name = "charakter", schema = "public")
public class Charakter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String chName;

//    @Value("1")
//    @Column(columnDefinition = "integer default 1")
    private Long chLevel;

    private Long hp;

    private Long damage;

    private Long classId;

    private Long raceId;

    private Long userId;

    private Long gameId;

    private String description;

    private String personality;
}
