package dd.mud.character.config.service.controll.service.repositoryservices;

import dd.mud.character.config.service.entity.model.Game;
import dd.mud.character.config.service.entity.repository.GameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameRepositoryService {

    private final GameRepository gameRepository;

    public List<Game> getGames () {
        return gameRepository.findAll();
    }
}