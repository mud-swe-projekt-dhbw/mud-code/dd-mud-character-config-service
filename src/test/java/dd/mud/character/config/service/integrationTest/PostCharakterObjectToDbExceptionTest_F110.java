package dd.mud.character.config.service.integrationTest;

import com.google.gson.Gson;
import dd.mud.character.config.service.boundary.model.CharakterDTO;
import dd.mud.character.config.service.controll.service.CharakterSaveService;
import dd.mud.character.config.service.controll.service.CheckGameIdService;
import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.GameRepositoryService;
import dd.mud.character.config.service.entity.model.Game;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class PostCharakterObjectToDbExceptionTest_F110 {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private CharakterRepositoryService charakterRepositoryService;

    @Mock
    private CheckGameIdService checkGameIdService;

    @Mock
    private GameRepositoryService gameRepositoryService;

    @InjectMocks
    private CharakterSaveService charakterSaveService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void postCharakterObjectToDB_ArgumentMissing_400 () throws Exception {
        CharakterDTO charakterDTO = new CharakterDTO(null, 1L, 1L, 1L, 1L, "desc", "pers");
        Gson gson = new Gson();

        mockMvc.perform(MockMvcRequestBuilders
                .post("/characterConfiguration/gameId/{game-id}/new", "200000")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(charakterDTO)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void postCharakterObjectToDB_EmptyString_400 () throws Exception {
        CharakterDTO charakterDTO = new CharakterDTO("", 1L, 1L, 1L, 1L, "desc", "pers");
        Gson gson = new Gson();

        mockMvc.perform(MockMvcRequestBuilders
                .post("/characterConfiguration/gameId/{game-id}/new", "200000")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(charakterDTO)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void postCharakterObjectToDB_falseGameId_404 () throws Exception {
        CharakterDTO charakterDTO = new CharakterDTO("name", 1L, 1L, 1L,  1L, "desc", "pers");
        Gson gson = new Gson();

        List<Game> gameList = new ArrayList<Game>(Arrays.asList(Game.builder().id(1L).gameName("game1").userId(1L).build(),
                Game.builder().id(2L).gameName("game2").userId(2L).build()));

        when(gameRepositoryService.getGames()).thenReturn(gameList);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/characterConfiguration/gameId/{game-id}/new", "200")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(charakterDTO)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void throwHttp400AfterInvalidInput() throws Exception {
        mockMvc.perform( MockMvcRequestBuilders
                .post("/characterConfiguration/gameId/{game-id}/new", "20000")
                .content("falseContent")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}
