package dd.mud.character.config.service.services;

import dd.mud.character.config.service.controll.service.repositoryservices.ItemRepositoryService;
import dd.mud.character.config.service.entity.model.Item;
import dd.mud.character.config.service.entity.repository.ItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

public class ItemRepositoryServiceTest_F110 {

    @Mock
    ItemRepository itemRepository;

    @InjectMocks
    ItemRepositoryService itemRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getItemNamesByGameId(){
        List<Item> itemList = new ArrayList<>(Arrays.asList(new Item(1L,"s", "s", 1, 1L)));
        when(itemRepository.findAllByGameId(anyLong())).thenReturn(itemList);
        List<Item> actuell = itemRepositoryService.getItemNamesByGameId(1L);
        assertEquals(itemList, actuell);

    }
}