package dd.mud.character.config.service.controll.service;

import dd.mud.character.config.service.controll.service.repositoryservices.UserRepositoryService;
import dd.mud.character.config.service.entity.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class CheckUserIdServiceTest_F110 {
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private CheckUserIdService checkUserIdService;

    @Mock
    private UserRepositoryService userRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void existsInDatabase_true(){
        List<User> userList = new ArrayList<>(Arrays.asList(User.builder().id(1000L).mail("mail").username("user1").mailHash("mailhash").passwordHash("pwh").registrationStatus(true).rank(3L).build()));
        when(userRepositoryService.getUsers()).thenReturn(userList);
        boolean actual = checkUserIdService.existsInDatabase(1000L);

        assertEquals(true, actual);
    }

    @Test
    void existsInDatabase_false(){
        List<User> userList = new ArrayList<>(Arrays.asList(User.builder().id(1000L).mail("mail").username("user1").mailHash("mailhash").passwordHash("pwh").registrationStatus(true).rank(3L).build()));
        when(userRepositoryService.getUsers()).thenReturn(userList);
        boolean actual = checkUserIdService.existsInDatabase(20L);

        assertEquals(false, actual);
    }
}