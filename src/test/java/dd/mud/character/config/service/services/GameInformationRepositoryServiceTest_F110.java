package dd.mud.character.config.service.services;

import dd.mud.character.config.service.boundary.model.ClassDTO;
import dd.mud.character.config.service.boundary.model.RaceDTO;
import dd.mud.character.config.service.controll.service.CheckGameIdService;
import dd.mud.character.config.service.controll.service.GameInformationRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.ClassRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.GameRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.RaceRepositoryService;
import dd.mud.character.config.service.entity.model.Class;
import dd.mud.character.config.service.entity.model.Race;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.when;

public class GameInformationRepositoryServiceTest_F110 {
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private GameInformationRepositoryService gameInformationRepositoryService;

    @Mock
    private ClassRepositoryService classRepositoryService;

    @Mock
    private RaceRepositoryService raceRepositoryService;

    @Mock
    private CheckGameIdService checkGameIdService;

    @Mock
    private GameRepositoryService gameRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getClassDTOByGameId(){
        List<ClassDTO> classDTOList = new ArrayList<>(Arrays.asList(ClassDTO.builder().classID(50L).className("class1").build(),
                ClassDTO.builder().classID(50L).className("class2").build()));

        List<Class> classList = new ArrayList<Class>(Arrays.asList(Class.builder().id(50L).className("class1").gameId(50L).build(),
                Class.builder().id(50L).className("class2").gameId(50L).build()));

        when(classRepositoryService.getClassNamesByGameId(50L)).thenReturn(classList);

        List<ClassDTO>  classDTOList1 = gameInformationRepositoryService.getClassDTOByGameId(50L);

        assertEquals(classDTOList, classDTOList1);
    }

    @Test
    void getRaceDTOByGameId(){
        List<RaceDTO> raceDTOList = new ArrayList<>(Arrays.asList(RaceDTO.builder().raceID(50L).raceName("class1").build(),
                RaceDTO.builder().raceID(50L).raceName("class2").build()));

        List<Race> raceList = new ArrayList<>(Arrays.asList(Race.builder().id(50L).raceName("class1").gameId(50L).build(),
                Race.builder().id(50L).raceName("class2").gameId(50L).build()));

        when(raceRepositoryService.getRaceNamesByGameId(50L)).thenReturn(raceList);

        List<RaceDTO>  raceDTOList1 = gameInformationRepositoryService.getRaceDTOByGameId(50L);

        assertEquals(raceDTOList, raceDTOList1);
    }

}
