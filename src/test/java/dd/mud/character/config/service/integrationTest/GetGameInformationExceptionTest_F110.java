package dd.mud.character.config.service.integrationTest;

import dd.mud.character.config.service.controll.service.CheckGameIdService;
import dd.mud.character.config.service.controll.service.GameInformationRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.ClassRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.GameRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.RaceRepositoryService;
import dd.mud.character.config.service.entity.model.Game;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class GetGameInformationExceptionTest_F110 {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private GameInformationRepositoryService gameInformationRepositoryService;

    @Mock
    private ClassRepositoryService classRepositoryService;

    @Mock
    private RaceRepositoryService raceRepositoryService;

    @Mock
    private CheckGameIdService checkGameIdService;

    @Mock
    private GameRepositoryService gameRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getGameInformation_gameIdIsString_404() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/characterConfiguration/gameId/{game-id}", "falseGameID"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void getGameInformation_gameIdDoesntExist_404 () throws Exception {
        List<Game> gameList = new ArrayList<Game>(Arrays.asList(Game.builder().id(1L).gameName("game1").userId(1L).build(),
                                                            Game.builder().id(2L).gameName("game2").userId(2L).build()));

        when(gameRepositoryService.getGames()).thenReturn(gameList);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/characterConfiguration/gameId/{game-id}", "100000000"))
                .andExpect(status().isNotFound())
                .andReturn();
    }
}
