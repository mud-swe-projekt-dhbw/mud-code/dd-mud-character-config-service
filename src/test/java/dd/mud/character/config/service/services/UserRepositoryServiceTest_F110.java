package dd.mud.character.config.service.services;

import dd.mud.character.config.service.controll.service.repositoryservices.GameRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.UserRepositoryService;
import dd.mud.character.config.service.entity.model.Game;
import dd.mud.character.config.service.entity.model.User;
import dd.mud.character.config.service.entity.repository.GameRepository;
import dd.mud.character.config.service.entity.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class UserRepositoryServiceTest_F110 {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserRepositoryService userRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getUsers(){
        List<User> users = new ArrayList<>(Arrays.asList(
                new User(5L, "testmail", "user1", "mailhash", "passwordhash",true, 2L),
                new User(1L, "testmail", "user1", "mailhash", "passwordhash",true, 2L)));

                when(userRepository.findAll()).thenReturn(users);
        assertEquals(users, userRepositoryService.getUsers());
    }
}
