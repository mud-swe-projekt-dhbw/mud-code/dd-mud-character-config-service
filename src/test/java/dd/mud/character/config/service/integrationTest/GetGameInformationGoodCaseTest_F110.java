package dd.mud.character.config.service.integrationTest;

import com.google.gson.Gson;
import dd.mud.character.config.service.boundary.model.ClassDTO;
import dd.mud.character.config.service.boundary.model.GameInformationDTO;
import dd.mud.character.config.service.boundary.model.RaceDTO;
import dd.mud.character.config.service.controll.service.CheckGameIdService;
import dd.mud.character.config.service.controll.service.GameInformationRepositoryService;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class GetGameInformationGoodCaseTest_F110 {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GameInformationRepositoryService gameInformationRepositoryService;

    @Test
    void getGameInformation() throws Exception {
        Long gameId = 100L;
        List<ClassDTO> classList = new ArrayList<>(Arrays.asList(ClassDTO.builder().classID(50L).className("class1").build(),
                ClassDTO.builder().classID(50L).className("class2").build()));
        List<RaceDTO> raceList = new ArrayList<>(Arrays.asList(RaceDTO.builder().raceID(50L).raceName("race1").build()));

        GameInformationDTO gameInformationDTO = GameInformationDTO.builder().classDTOList(classList).raceDTOList(raceList).build();

        when(gameInformationRepositoryService.getGameInformationByGameId(gameId)).thenReturn(gameInformationDTO);

        Gson gson = new Gson();

        mockMvc.perform(MockMvcRequestBuilders
                .get("/characterConfiguration/gameId/{gameId}", "100")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(gson.toJson(gameInformationDTO)));
    }
}
