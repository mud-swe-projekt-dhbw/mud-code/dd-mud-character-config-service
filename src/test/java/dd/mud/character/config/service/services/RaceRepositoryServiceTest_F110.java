package dd.mud.character.config.service.services;

import dd.mud.character.config.service.controll.service.repositoryservices.RaceRepositoryService;
import dd.mud.character.config.service.entity.model.Race;
import dd.mud.character.config.service.entity.repository.RaceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class RaceRepositoryServiceTest_F110 {

    @Mock
    RaceRepository raceRepository;

    @InjectMocks
    RaceRepositoryService raceRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getRacesByGameId(){
        Long gameId = 50L;
        List<Race> raceList = new ArrayList<Race>(Arrays.asList(
                Race.builder().raceName("race1").gameId(gameId).build(),
                Race.builder().raceName("race2").gameId(gameId).build()));

        when(raceRepository.findAllByGameId(gameId)).thenReturn(raceList);
        assertEquals(raceList, raceRepositoryService.getRaceNamesByGameId(gameId));
    }
}
