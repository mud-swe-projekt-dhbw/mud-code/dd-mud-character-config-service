package dd.mud.character.config.service.services;

import dd.mud.character.config.service.controll.service.repositoryservices.GameRepositoryService;
import dd.mud.character.config.service.entity.model.Game;
import dd.mud.character.config.service.entity.repository.GameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class GameRepositoryServiceTest_F110 {

    @Mock
    GameRepository gameRepository;

    @InjectMocks
    GameRepositoryService gameRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getGames(){
        List<Game> games = new ArrayList<Game>(Arrays.asList(
                Game.builder().gameName("game1").userId(5L).build(),
                Game.builder().gameName("game2").userId(50L).build()));
        when(gameRepository.findAll()).thenReturn(games);
        assertEquals(games, gameRepositoryService.getGames());
    }
}
