package dd.mud.character.config.service.integrationTest;

import com.google.gson.Gson;
import dd.mud.character.config.service.boundary.model.CharakterDTO;
import dd.mud.character.config.service.controll.service.CharakterSaveService;
import dd.mud.character.config.service.controll.service.CheckGameIdService;
import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.GameRepositoryService;
import dd.mud.character.config.service.entity.model.Charakter;
import dd.mud.character.config.service.entity.model.Game;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class PostCharakterObjectToDbGoodCaseTest_F110 {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CharakterRepositoryService charakterRepositoryService;

    @MockBean
    private CheckGameIdService checkGameIdService;

    @MockBean
    private GameRepositoryService gameRepositoryService;

    @MockBean
    private CharakterSaveService charakterSaveService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void postCharakterObjectToDB_201 () throws Exception {
        CharakterDTO charakterDTO = new CharakterDTO("chName", 1L, 1L, 1L, 1L, "desc", "pers");
        List<Game> gameList = new ArrayList<>(Arrays.asList(Game.builder().id(200L).gameName("game1").userId(1L).build()));
        Gson gson = new Gson();
//        when(gameRepositoryService.getGames()).thenReturn(gameList);
        doReturn(gameList).when(gameRepositoryService).getGames();
        Mockito.doNothing().when(charakterRepositoryService).saveNewCharakter(any(Charakter.class));

        mockMvc.perform(MockMvcRequestBuilders
                .post("/characterConfiguration/gameId/{game-id}/new", "200")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(charakterDTO)))
                .andExpect(status().isCreated())
                .andReturn();
    }
}