package dd.mud.character.config.service.services;

import dd.mud.character.config.service.controll.service.repositoryservices.ClassRepositoryService;
import dd.mud.character.config.service.entity.model.Class;
import dd.mud.character.config.service.entity.repository.ClassRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class ClassRepositoryServiceTest_F110 {

    @Mock
    ClassRepository classRepository;

    @InjectMocks
    ClassRepositoryService classRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getClassesByGameId(){
        Long gameId = 50L;
        List<Class> classList = new ArrayList<Class>(Arrays.asList(
                Class.builder().className("class1").gameId(gameId).build(),
                Class.builder().className("class2").gameId(gameId).build()));

        when(classRepository.findAllByGameId(gameId)).thenReturn(classList);
        assertEquals(classList, classRepositoryService.getClassNamesByGameId(gameId));
    }
}
