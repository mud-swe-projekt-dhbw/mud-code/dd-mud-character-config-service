package dd.mud.character.config.service.integrationTest;

import com.google.gson.Gson;
import dd.mud.character.config.service.boundary.model.CharakterAlreadyExistsDTO;
import dd.mud.character.config.service.controll.service.CharakterAlreadyExistsService;
import dd.mud.character.config.service.controll.service.CheckGameIdService;
import dd.mud.character.config.service.controll.service.CheckUserIdService;
import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.GameRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.UserRepositoryService;
import dd.mud.character.config.service.entity.model.Game;
import dd.mud.character.config.service.entity.model.User;
import dd.mud.character.config.service.entity.repository.CharakterRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class GetCharakterAlreadyExistsGoodCaseTest_F110 {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CharakterAlreadyExistsService charakterAlreadyExistsService;

    @MockBean
    private CharakterRepository charakterRepository;

    @MockBean
    private CheckGameIdService checkGameIdService;

    @MockBean
    private CheckUserIdService checkUserIdService;

    @MockBean
    private GameRepositoryService gameRepositoryService;

    @MockBean
    private UserRepositoryService userRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void charakterAlreadyExists() throws Exception {
        Gson gson = new Gson();
        CharakterAlreadyExistsDTO charakterAlreadyExistsDTO = new CharakterAlreadyExistsDTO(true);

        when(checkUserIdService.existsInDatabase(anyLong())).thenReturn(true);
        when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(true);
        when(charakterRepository.existsByUserIdAndGameId(anyLong(), anyLong())).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/characterConfiguration/gameId/{game-id}/playerId/{player-id}/characterExistance", "1000", "1000")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
//                .andExpect(content().json(gson.toJson(charakterAlreadyExistsDTO)))
    }
}
