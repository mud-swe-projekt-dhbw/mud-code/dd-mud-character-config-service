package dd.mud.character.config.service.controll.service;

import com.google.gson.Gson;
import dd.mud.character.config.service.boundary.model.CharakterAlreadyExistsDTO;
import dd.mud.character.config.service.boundary.model.CharakterDTO;
import dd.mud.character.config.service.controll.exceptions.IllegalCharakterInformationException;
import dd.mud.character.config.service.controll.exceptions.IllegalGameIdException;
import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.ItemCharakterRepositoryService;
import dd.mud.character.config.service.entity.model.Charakter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class CharakterSaveServiceTest_F110 {
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private CharakterSaveService charakterSaveService;

    @Mock
    private CheckGameIdService checkGameIdService;

    @Mock
    private CharakterRepositoryService charakterRepositoryService;

    @Mock
    private ItemCharakterRepositoryService itemCharakterRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void buildCharakter() {
        Long gameId = 200L;
        Long classId = 200L;
        Long userId = 200L;
        Long raceId = 200L;
        Long itemId = 200L;
        String chName = "name";
        String desc = "desc";
        String perso = "perso";
        CharakterDTO charakterDTO = new CharakterDTO(chName, classId, raceId, itemId, userId, desc, perso);
        Charakter expectedCharakter = Charakter.builder().chName(chName).chLevel(null).hp(null).damage(null).classId(classId).raceId(raceId).userId(userId).gameId(gameId).description(desc).personality(perso).build();
        Charakter actualCharakter = charakterSaveService.buildCharakter(charakterDTO, gameId);
        assertEquals(expectedCharakter, actualCharakter);
    }

    @Test
    void saveCharakterToBD_CharakterWithNull_throwsIllegalCharakterInformationException (){
        Long gameId = 200L;
        CharakterDTO charakterDTO = CharakterDTO.builder().itemId(null).build();
        assertThrows(IllegalCharakterInformationException.class, () -> charakterSaveService.saveCharakterToDB(charakterDTO, gameId));
    }

    @Test
    void saveCharakterToDB_CharakterWithEmptyString_throwsIllegalCharakterInformationException(){
        Long gameId = 200L;
        CharakterDTO charakterDTO = CharakterDTO.builder().chName("").itemId(1L).classId(1L).description("sd").personality("sd").raceId(1L).userId(1L).build();
        assertThrows(IllegalCharakterInformationException.class, () -> charakterSaveService.saveCharakterToDB(charakterDTO, gameId));
    }

    @Test
    void saveCharakterToDB_GameIdDoesntExists_throwsIllegalGameIdException(){
        Long gameId = 200L;
        CharakterDTO charakterDTO = CharakterDTO.builder().chName("name").itemId(1L).classId(1L).description("sd").personality("sd").raceId(1L).userId(1L).build();
        when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(false);
        assertThrows(IllegalGameIdException.class, () -> charakterSaveService.saveCharakterToDB(charakterDTO, gameId));
    }

    @Test
    void saveCharakterToDB_GoodCase(){
        Long gameId = 200L;
        CharakterDTO charakterDTO = CharakterDTO.builder().chName("name").itemId(1L).classId(1L).description("sd").personality("sd").raceId(1L).userId(1L).build();
        when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(true);
        when(charakterRepositoryService.getCharakterByUserIdAndGameId(anyLong(), anyLong())).thenReturn(Charakter.builder().id(2L).build());
        Mockito.doNothing().when(charakterRepositoryService).saveNewCharakter(any(Charakter.class));
        Mockito.doNothing().when(itemCharakterRepositoryService).saveNewItemIdCharakterId(anyLong(),anyLong(), anyLong());
        assertTrue(true);
    }

}