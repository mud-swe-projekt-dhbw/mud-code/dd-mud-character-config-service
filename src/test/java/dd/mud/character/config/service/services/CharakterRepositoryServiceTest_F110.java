package dd.mud.character.config.service.services;

import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import dd.mud.character.config.service.entity.model.Charakter;
import dd.mud.character.config.service.entity.repository.CharakterRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class CharakterRepositoryServiceTest_F110 {

    @Mock
    CharakterRepository charakterRepository;

    @InjectMocks
    CharakterRepositoryService charakterRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveNewCharakter() {
        Long gameId = 50L;
        String charakterName = "TestCharakter";
        Long hp = 50L;
        Long damage = 50L;
        String description ="TestDescription";
        String personality ="TestPersonality";

        Charakter charakter = Charakter.builder().gameId(gameId).chName(charakterName).hp(hp).damage(damage)
                .description(description).personality(personality).build();

        when(charakterRepository.save(charakter)).thenReturn(charakter);

        charakterRepositoryService.saveNewCharakter(charakter);

        verify(charakterRepository, times(1)).save(charakter);
    }

    @Test
    void getAllCharakters(){
        String charakterName1 = "charakter1";
        String charakterName2 = "charakter2";
        List<Charakter> charakters = new ArrayList<Charakter>(Arrays.asList(
                Charakter.builder().chName(charakterName1).userId(5L).build(),
                Charakter.builder().chName(charakterName2).userId(50L).build()));

        when(charakterRepository.findAll()).thenReturn(charakters);
        assertEquals(charakters, charakterRepositoryService.getAllCharakters());
    }

    @Test
    void getCharakterByGameIdAndUserId() {
        Long userId = 100L;
        Long gameId = 50L;

        when(charakterRepository.existsByUserIdAndGameId(userId, gameId)).thenReturn(true);
        boolean actual = charakterRepositoryService.existCharakterByGameIdAndUserId(userId, gameId);


        assertTrue(actual);
    }

    @Test
    void getAllCharaktersByUserIdAndGameId(){
        Charakter charakter = Charakter.builder().chName("charakterName1").userId(5L).build();

        when(charakterRepository.getCharakterByUserIdAndGameId(anyLong(),anyLong())).thenReturn(charakter);
        assertEquals(charakter, charakterRepositoryService.getCharakterByUserIdAndGameId(1L,1L));
    }

    @Test
    void deleteCharakterByGameId(){
        Mockito.doNothing().when(charakterRepository).deleteById(anyLong());

        charakterRepositoryService.deleteCharakterById(1L);

        verify(charakterRepository, times(1)).deleteById(1L);
    }
}
