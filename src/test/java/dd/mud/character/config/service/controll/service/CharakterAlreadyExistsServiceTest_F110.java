package dd.mud.character.config.service.controll.service;

import dd.mud.character.config.service.boundary.model.CharakterAlreadyExistsDTO;
import dd.mud.character.config.service.controll.exceptions.IllegalGameIdException;
import dd.mud.character.config.service.controll.exceptions.IllegalUserIdException;
import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class CharakterAlreadyExistsServiceTest_F110 {
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private CharakterAlreadyExistsService charakterAlreadyExistsService;

    @Mock
    private CheckGameIdService checkGameIdService;

    @Mock
    private CheckUserIdService checkUserIdService;

    @Mock
    private CharakterRepositoryService charakterRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void existsInDatabase_true(){
       when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(true);
       when(checkUserIdService.existsInDatabase(anyLong())).thenReturn(true);
       when(charakterRepositoryService.existCharakterByGameIdAndUserId(anyLong(),anyLong())).thenReturn(true);
       CharakterAlreadyExistsDTO charakterAlreadyExistsDTO = charakterAlreadyExistsService.charakterAlreadyExists(500L,600L);

        assertTrue(charakterAlreadyExistsDTO.isCharakterAlreadyExists());
    }

    @Test
    void existsInDatabase_throwsIlegalGameIdException(){
        when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(false);
        assertThrows(IllegalGameIdException.class, () -> charakterAlreadyExistsService.charakterAlreadyExists(500L,600L));
    }

    @Test
    void existsInDatabase_throwsIlegalUserIdException(){
        when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(true);
        when(checkUserIdService.existsInDatabase(anyLong())).thenReturn(false);
        assertThrows(IllegalUserIdException.class, () -> charakterAlreadyExistsService.charakterAlreadyExists(500L,600L));
    }

}