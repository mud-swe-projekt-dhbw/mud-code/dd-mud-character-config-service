package dd.mud.character.config.service.controll.service;

import com.google.gson.Gson;
import dd.mud.character.config.service.boundary.model.ClassDTO;
import dd.mud.character.config.service.boundary.model.GameInformationDTO;
import dd.mud.character.config.service.boundary.model.ItemDTO;
import dd.mud.character.config.service.boundary.model.RaceDTO;
import dd.mud.character.config.service.controll.service.repositoryservices.ClassRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.ItemRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.RaceRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.UserRepositoryService;
import dd.mud.character.config.service.entity.model.Class;
import dd.mud.character.config.service.entity.model.Item;
import dd.mud.character.config.service.entity.model.Race;
import dd.mud.character.config.service.entity.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class GameInformationRepositoryServiceTest_F110 {
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private GameInformationRepositoryService gameInformationRepositoryService;

    @Mock
    private CheckGameIdService checkGameIdService;

    @Mock
    private UserRepositoryService userRepositoryService;

    @Mock
    private CheckUserIdService checkUserIdService;

    @Mock
    private ClassRepositoryService classRepositoryService;

    @Mock
    private RaceRepositoryService raceRepositoryService;

    @Mock
    private ItemRepositoryService itemRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void existsInDatabase_false() {
        List<Class> classList = new ArrayList<>(Arrays.asList(new Class(200L, "class1", 200L)));
        List<Race> raceList = new ArrayList<>(Arrays.asList(new Race(200L, "race1", 200L)));
        List<Item> itemList = new ArrayList<>(Arrays.asList(new Item(200L, "itemType1", "item1", 1, 200L)));
        List<ClassDTO> classDTOList = new ArrayList<>(Arrays.asList(new ClassDTO(200L, "class1")));
        List<RaceDTO> raceDTOList = new ArrayList<>(Arrays.asList(new RaceDTO(200L, "race1")));
        List<ItemDTO> itemDTOList = new ArrayList<>(Arrays.asList(new ItemDTO("item1", 200L)));
        GameInformationDTO expectedGameInformationDTO = new GameInformationDTO(classDTOList, raceDTOList, itemDTOList);

        when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(true);
        when(classRepositoryService.getClassNamesByGameId(anyLong())).thenReturn(classList);
        when(raceRepositoryService.getRaceNamesByGameId(anyLong())).thenReturn(raceList);
        when(itemRepositoryService.getItemNamesByGameId(anyLong())).thenReturn(itemList);
        GameInformationDTO actualGameInformationDTO = gameInformationRepositoryService.getGameInformationByGameId(50L);

//        assertEquals(expectedGameInformationDTO,actualGameInformationDTO);
        assertEquals(expectedGameInformationDTO.getClassDTOList(), actualGameInformationDTO.getClassDTOList());
        assertEquals(expectedGameInformationDTO.getRaceDTOList(), actualGameInformationDTO.getRaceDTOList());
    }
}