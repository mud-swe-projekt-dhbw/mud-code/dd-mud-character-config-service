package dd.mud.character.config.service.controll.service;

import dd.mud.character.config.service.controll.service.repositoryservices.GameRepositoryService;
import dd.mud.character.config.service.entity.model.Game;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class CheckGameIdServiceTest_F110 {
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private CheckGameIdService checkGameIdService;

    @Mock
    private GameRepositoryService gameRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void existsInDatabase_true(){
        List<Game> gameList = new ArrayList<Game>(Arrays.asList(Game.builder().id(100L).gameName("game1").userId(1L).build(),
                Game.builder().id(2L).gameName("game2").userId(200L).build()));

        when(gameRepositoryService.getGames()).thenReturn(gameList);
        boolean actual = checkGameIdService.existsInDataBase(100L);

        assertEquals(true, actual);
    }

    @Test
    void existsInDatabase_false(){
        List<Game> gameList = new ArrayList<Game>(Arrays.asList(Game.builder().id(100L).gameName("game1").userId(1L).build(),
                Game.builder().id(2L).gameName("game2").userId(200L).build()));
        when(gameRepositoryService.getGames()).thenReturn(gameList);
        boolean actual = checkGameIdService.existsInDataBase(20L);

        assertEquals(false, actual);
    }

}