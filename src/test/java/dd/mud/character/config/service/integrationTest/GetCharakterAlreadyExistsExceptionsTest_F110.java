package dd.mud.character.config.service.integrationTest;

import dd.mud.character.config.service.controll.service.CharakterAlreadyExistsService;
import dd.mud.character.config.service.controll.service.CheckGameIdService;
import dd.mud.character.config.service.controll.service.CheckUserIdService;
import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.GameRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.UserRepositoryService;
import dd.mud.character.config.service.entity.model.Game;
import dd.mud.character.config.service.entity.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class GetCharakterAlreadyExistsExceptionsTest_F110 {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private CharakterAlreadyExistsService charakterAlreadyExistsService;

    @Mock
    private CharakterRepositoryService charakterRepositoryService;

    @Mock
    private CheckGameIdService checkGameIdService;

    @Mock
    private CheckUserIdService checkUserIdService;

    @Mock
    private GameRepositoryService gameRepositoryService;

    @Mock
    private UserRepositoryService userRepositoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void charakterAlreadyExists_falseUserId_404() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/characterConfiguration/gameId/{game-id}/playerId/{player-id}/characterExistance", "10000", "falseUserId"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void charakterAlreadyExists_userIdDoesntExist_404 () throws Exception {
        List<User> userList = new ArrayList<>(Arrays.asList(User.builder().id(5L).mail("mail").username("user1").mailHash("mailhash").passwordHash("pwh").registrationStatus(true).rank(3L).build()));

        when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(true);
        when(userRepositoryService.getUsers()).thenReturn(userList);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/characterConfiguration/gameId/{game-id}/playerId/{player-id}/characterExistance", "100000", "10000"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void charakterAlreadyExists_falseGameId_404() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/characterConfiguration/gameId/{game-id}/playerId/{player-id}/characterExistance", "falseGameId", "10000000"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void charakterAlreadyExists_gameIdDoesntExist_404 () throws Exception {
        List<Game> gameList = new ArrayList<>(Arrays.asList(Game.builder().id(4L).gameName("game1").userId(4L).build()));

        when(gameRepositoryService.getGames()).thenReturn(gameList);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/characterConfiguration/gameId/{game-id}/playerId/{player-id}/characterExistance", "1000000000", "1000000"))
                .andExpect(status().isNotFound())
                .andReturn();
    }
}
