package dd.mud.character.config.service.controll.service;

import dd.mud.character.config.service.controll.exceptions.IllegalGameIdException;
import dd.mud.character.config.service.controll.exceptions.IllegalUserIdException;
import dd.mud.character.config.service.controll.service.repositoryservices.CharakterRepositoryService;
import dd.mud.character.config.service.controll.service.repositoryservices.ItemCharakterRepositoryService;
import dd.mud.character.config.service.entity.model.Charakter;
import dd.mud.character.config.service.entity.model.ItemCharakter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class DeleteCharakterServiceTest_F110 {
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private DeleteCharakterService deleteCharakterService;

    @Mock
    private CheckGameIdService checkGameIdService;

    @Mock
    private CharakterRepositoryService charakterRepositoryService;

    @Mock
    private CheckUserIdService checkUserIdService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void deleteCharakterWithGameIdAndPlayerId_WrongGameId_ThrowsIllegalGameIdException(){
        when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(false);
        assertThrows(IllegalGameIdException.class, () -> deleteCharakterService.deleteCharakterWithGameIdAndPlayerId(1L, 1L));
    }

    @Test
    void deleteCharakterWithGameIdAndPlayerId_WrongUserId_ThrowsIllegalUserIdException(){
        when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(true);
        when(checkUserIdService.existsInDatabase(anyLong())).thenReturn(false);
        assertThrows(IllegalUserIdException.class, () -> deleteCharakterService.deleteCharakterWithGameIdAndPlayerId(1L, 1L));
    }

    @Test
    void deleteCharakterWithGameIdAndPlayerId_WrongUserIdGameIdCombination_ThrowsIllegalGameIdException(){
        when(checkGameIdService.existsInDataBase(anyLong())).thenReturn(true);
        when(checkUserIdService.existsInDatabase(anyLong())).thenReturn(true);
        when(charakterRepositoryService.existCharakterByGameIdAndUserId(anyLong(),anyLong())).thenReturn(false);
        assertThrows(IllegalGameIdException.class, () -> deleteCharakterService.deleteCharakterWithGameIdAndPlayerId(1L, 1L));
    }
}



//    public void deleteCharakterWithGameIdAndPlayerId(Long gameId, Long playerId){
//        if(!(checkGameIdService.existsInDataBase(gameId))){
//            throw new IllegalGameIdException();
//        }
//        if(!(checkUserIdService.existsInDatabase(playerId))){
//            throw new IllegalUserIdException();
//        }
//        if(!(charakterRepositoryService.existCharakterByGameIdAndUserId(gameId, playerId))){
//            throw new IllegalGameIdException();
//        }
//        Charakter charakter = charakterRepositoryService.getCharakterByUserIdAndGameId(playerId, gameId);
//        List<ItemCharakter> itemCharakterList = itemCharakterRepositoryService.getItemCharakterByChId(charakter.getId());
//        for (ItemCharakter itemCharakter: itemCharakterList) {
//            itemCharakterRepositoryService.deleteItemIdCharakterId(charakter.getId(), itemCharakter.getItemId());
//        }
//        charakterRepositoryService.deleteCharakterById(charakter.getId());
//    }